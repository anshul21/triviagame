import React from 'react';
import MyContext from '../Context';
import '../Styles/questions.css'
class Questions extends React.Component {
    questionNumber = this.context.state.questionNumber;
    correctAnswer = this.context.state.results.results[this.questionNumber].correct_answer;

    rightAnswer() {
        this.context.state.increaseQuestion();
        this.context.state.increaseScore();
    }

    wrongAnswer() {
        this.context.state.increaseQuestion();
    }

    checkResult(e) {
        let checkCorrectAnswer = this.context.state.results.results[this.context.state.questionNumber].correct_answer;
        if (e.target.value == checkCorrectAnswer) {
            console.log("write answer");
            this.rightAnswer();
        }
        else {
            console.log("wrong answer");
            this.wrongAnswer();
        }
    }

    getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }


    suffle(incorrectQuestions, correctAnswer) {
        let answerArray = [correctAnswer];
        for (let iterator = 0; iterator < 3; iterator++) {
            answerArray.push(incorrectQuestions[iterator]);
        }
        let suffleNumber = this.getRandomInt(4);
        let tempVariable = answerArray[suffleNumber];
        answerArray[suffleNumber] = answerArray[0];
        answerArray[0] = tempVariable;

        return answerArray;
    }



    render() {
        let questionNumber = this.context.state.questionNumber;

        if (questionNumber < this.context.state.results.results.length) {
            let correctAnswer = this.context.state.results.results[questionNumber].correct_answer;
            let inCorrectAnswer = this.context.state.results.results[questionNumber].incorrect_answers;
            let answerArray = this.suffle(inCorrectAnswer, correctAnswer);

            return (
                <div className="questionAnswer">
                    <div className="question">
                        <button >{this.context.state.results.results[questionNumber].question}</button>
                    </div>
                    <div className="container answers">
                        <div className="row">
                            <div className="col">
                                <button value={answerArray[0]} onClick={this.checkResult.bind(this)}>{answerArray[0]}</button>
                            </div>
                            <div className="col">
                                <button value={answerArray[1]} onClick={this.checkResult.bind(this)}>{answerArray[1]}</button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <button value={answerArray[2]} onClick={this.checkResult.bind(this)}>{answerArray[2]}</button>
                            </div>
                            <div className="col">
                                <button value={answerArray[3]} onClick={this.checkResult.bind(this)}>{answerArray[3]}</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div>
                    <h1>Your Score Is</h1>
                    <h2>{this.context.state.score}</h2>
                </div>
            );

        }
    }
}
Questions.contextType = MyContext;
export default Questions;
