import MyContext from '../Context';
import React from 'react';
import axios from 'axios'
const API = 'https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple'
class MyProvider extends React.Component {
  constructor() {
    super();
    this.fetchData();
    this.state = {
      results: [],
      isLoaded: false,
      questionNumber: 0,
      increaseQuestion: () => {
        this.setState({ questionNumber: this.state.questionNumber + 1 })
      },
      score: 0,
      increaseScore: () => {
        this.setState({ score: this.state.score + 1 })
      },
      error: null,
      isError: false
    }

  }

  setError(error) {
    this.setState({ error: error });
    this.setState({ isError: true });
  }

  fetchData() {
    axios.get(API)
      .then(res => {
        if (res.data.results) {
          this.setState({ results: res.data });
          this.setState({ isLoaded: true });
          console.log(res.data.results);
        }
      })
      .catch(error => this.setError(error));
  }

  render() {
    return (
      <MyContext.Provider
        value={{ state: this.state }}>
        {this.props.children}
      </MyContext.Provider>

    )
  }
}


export default MyProvider;