import React from 'react';
import './App.css';
import Questions from './Views/Questions'
import Banner from './Views/Banner'
import MyContext from './Context';
import 'bootstrap/dist/css/bootstrap.min.css'
class App extends React.Component {

  render() {

    if (this.context.state.isLoaded == true) {
      return (
        <div className="container App">
          <Banner></Banner>
          <Questions></Questions>
        </div>
      );
    }
    else {
      if (this.context.state.isError == true) {
        return (
          <div className="container App">
            <div>Somethink went wrong Please try again</div>
          </div>);
      }
      else {
        return (
          <div className="container App">
            <Banner></Banner>
            <div>Loading Your Game</div>
          </div>
        );
      }
    }
  }
}
App.contextType = MyContext;
export default App;
